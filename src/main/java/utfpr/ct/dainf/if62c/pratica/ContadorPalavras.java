/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author kelvin
 */
public class ContadorPalavras {
    private BufferedReader reader;
    public ContadorPalavras(String caminho) throws FileNotFoundException{
        this.reader = new BufferedReader(new FileReader(caminho));
    }
    public HashMap getPalavras() throws IOException{
        String linha;
        String palavra = "";
        int qtd;
        HashMap<String,Integer> mapaDePalavras = new HashMap<String , Integer>();
        while((linha = reader.readLine()) != null){
            palavra = "";
            for(int i = 0;i < linha.length();i++){
                if(linha.charAt(i) >= 65 && linha.charAt(i) <= 90 || linha.charAt(i) >= 97 && linha.charAt(i) <= 122){
                    palavra += linha.charAt(i);
                }
                else if(!palavra.equals("")){
                    if(mapaDePalavras.containsKey(palavra)){
                        qtd = mapaDePalavras.get(palavra) + 1;
                        mapaDePalavras.put(palavra,qtd);
                    }
                    else{
                        mapaDePalavras.put(palavra, 1);
                    }
                    palavra = "";
                }
                if(i == (linha.length() - 1)){
                    if(mapaDePalavras.containsKey(palavra)){
                        qtd = mapaDePalavras.get(palavra) + 1;
                        mapaDePalavras.put(palavra,qtd);
                    }
                    else{
                        mapaDePalavras.put(palavra, 1);
                    }
                }
            }
        }
        reader.close();
        return mapaDePalavras;
    }
}
