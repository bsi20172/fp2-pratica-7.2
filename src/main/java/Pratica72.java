
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    public static class Palavra implements Comparable<Palavra>{
        int repeticoes;
        String palavra;
        public Palavra(String palavra,Integer repeticoes){
            this.palavra = palavra;
            this.repeticoes = repeticoes;
        }
        @Override
        public int compareTo(Palavra palavra){
            return (repeticoes - palavra.repeticoes) * -1;
        }
    }
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("digite o caminho do arquivo .txt");
        String teste;
        Scanner scanner = new Scanner(System.in);
        String caminho = scanner.next();
        ContadorPalavras contador = new ContadorPalavras(caminho);
        HashMap<String,Integer> mapaDePalavras = contador.getPalavras();
        List<Palavra> lista = new ArrayList<>();
        Set<String> chaves = mapaDePalavras.keySet();
        for(String aux : chaves){
            lista.add(new Palavra(aux,mapaDePalavras.get(aux)));
        }
        sort(lista);
        BufferedWriter saida = new BufferedWriter(new FileWriter(caminho + ".out"));
        for(Palavra aux1 : lista){
            teste = aux1.palavra + "," + aux1.repeticoes;
            System.out.println(teste);
            saida.write(teste);
            saida.newLine();
        }
        saida.close();
    }
}
